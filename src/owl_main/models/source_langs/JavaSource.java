package owl_main.models.source_langs;

import owl_main.models.Syntax;

public class JavaSource extends SourceLanguage {

    public JavaSource() {

        conversions.put(Syntax.FOR, "for");
        conversions.put(Syntax.WHILE, "while");

        conversions.put(Syntax.OPENING_ENCLOSE, "(");
        conversions.put(Syntax.CLOSING_ENCLOSE, ")");

        conversions.put(Syntax.OPENING_ARRAY, "[");
        conversions.put(Syntax.CLOSING_ARRAY, "]");

        conversions.put(Syntax.INT, "int");
        conversions.put(Syntax.FLOAT, "float");
        conversions.put(Syntax.STRING, "String");
        conversions.put(Syntax.DOUBLE, "double");
        conversions.put(Syntax.BOOLEAN, "boolean");
        conversions.put(Syntax.SHORT, "short");
        conversions.put(Syntax.LONG, "long");
        conversions.put(Syntax.CHAR, "char");
        conversions.put(Syntax.OBJECT, "Object");

        conversions.put(Syntax.LINE_END, ";");
        conversions.put(Syntax.ASSIGNMENT_OPERATOR, "=");

    }
}
