package owl_main.models.source_langs;

import owl_main.models.Syntax;

import java.util.HashMap;

public abstract class SourceLanguage {


    public HashMap<Enum, String> conversions = new HashMap<>();

    public SourceLanguage() {

        conversions.put(Syntax.FOR, "");
        conversions.put(Syntax.WHILE, "");

        conversions.put(Syntax.OPENING_ENCLOSE, "");
        conversions.put(Syntax.CLOSING_ENCLOSE, "");

        conversions.put(Syntax.OPENING_ARRAY, "");
        conversions.put(Syntax.CLOSING_ARRAY, "");

        conversions.put(Syntax.INT, "");
        conversions.put(Syntax.FLOAT, "");
        conversions.put(Syntax.STRING, "");
        conversions.put(Syntax.DOUBLE, "");
        conversions.put(Syntax.BOOLEAN, "");
        conversions.put(Syntax.SHORT, "");
        conversions.put(Syntax.LONG, "");
        conversions.put(Syntax.CHAR, "");
        conversions.put(Syntax.OBJECT, "");

        conversions.put(Syntax.LINE_END, "");
        conversions.put(Syntax.ASSIGNMENT_OPERATOR, "");

    }
}
