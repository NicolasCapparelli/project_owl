package owl_main.models;

import owl_main.models.schemas.TranspilerSchema;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Transpiler {

    private TranspilerSchema schema;
    private ArrayList<String> types = new ArrayList<>(Arrays.asList("int", "float", "String", "double", "boolean", "short", "long", "char"));
    private ArrayList<String> repetitionKeywords = new ArrayList<>(Arrays.asList("for", "while", "do"));
    private ArrayList<String> selectionKeywords = new ArrayList<>(Arrays.asList("if", "else", "if else"));
    private ArrayList<String> regexStrings = new ArrayList<>();

    // Yes I know these variables are redundant but it's easier to understand and append stuff this way
    private String regexTypes = "\\b(int|float|String|double|boolean|short|long|char)\\b";
    private String regexRepetition = "\\b(for|while|do)\\b";
    private String regexSelection = "\\b(if|else|if else|switch|case)\\b";
    private String regexBlockEnds = "\\b(\\{|\\})\\b";
    private Pattern firstWordRegexPattern = Pattern.compile("(?:^|(?:[.!?]\\s))(\\w+)|(?<!.)[{}](?!.)");
    private Pattern fallBackFirstWordPattern = Pattern.compile("\\w+\\b"); // More lax than the first one, as a fallback for some edge-cases

    public Transpiler(TranspilerSchema s){
        this.schema = s;
        regexStrings.add(regexTypes);
        regexStrings.add(regexRepetition);
        regexStrings.add(regexSelection);
        regexStrings.add(regexBlockEnds);


    }

    public String cleanSource(String sourceCode){
        return this.schema.cleanCode(sourceCode);
    }

    public String transpile(String[] source){
        StringBuilder output = new StringBuilder();

        int spacesRemoved;

        for (String line : source){

            spacesRemoved = line.indexOf(line.trim());
            this.schema.currentLineSpacesRemoved = spacesRemoved;

            line = cleanSource(line);

            line = line.trim();

            String finalLine = String.join("", Collections.nCopies(spacesRemoved, " ")) + firstWordSearch(line, output);

            // ONLY RETURN NULL IF: you want the line completely omitted from the output
            if (!finalLine.trim().equals("null")){

                // Once regexSearch has found a match it will return true and break the loop
                output.append(finalLine);
                output.append("\n");
            }

        }

        // Trim removes any whitespace on top caused due to import or package lines
        return output.toString().trim();
    }

    private String firstWordSearch(String line, StringBuilder output){

        Matcher rMatcher = firstWordRegexPattern.matcher(line);
        Matcher sMatcher = fallBackFirstWordPattern.matcher(line);

        if (rMatcher.find()){
            System.out.println("LINE:" + line);
            System.out.println("MATCH: " + rMatcher.group(0));
            return schema.getConversion(rMatcher.group(0).trim(), line);

        } else if (sMatcher.find() && schema.guide.containsKey(sMatcher.group(0))) {
            return schema.getConversion(sMatcher.group(0).trim(), line);

        } else {
            return schema.lastDitchConversion(line);
        }
    }

    public void changeSchema(TranspilerSchema s){
        this.schema = s;
    }

}
