package owl_main.models.schemas;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KsuPseudocodeSchema extends TranspilerSchema {

    private CONVERSION lastValidConversion = CONVERSION.NIL;

    private Pattern firstWordRegexPattern = Pattern.compile("(?:^|(?:[.!?]\\s))(\\w+)|([{}])");
    private Pattern behindAssignmentOperatorRegexPattern = Pattern.compile(".+?(?==)");
    private Pattern parameterRegexPattern = Pattern.compile("(?<=\\().+?(?=\\))");
    private Pattern withinParenthesesTypeFinderPattern = Pattern.compile("\\w+\\b(?![,)])"); // Must be used after using parameterRegexPattern

    private ArrayList<String> primitiveTypes = new ArrayList<String>(Arrays.asList("byte", "short", "int","long", "char", "float", "double", "boolean"));

    public KsuPseudocodeSchema(){
    }

    @Override
    public String getConversion(String match, String line) {

        if (guide.containsKey(match)){
            return this.guide.get(match).apply(line);
        }

        else {
            return edgeCaseAttempt(line);
        }
    }

    @Override
    public String cleanCode(String sourceCode) {

        return sourceCode.replaceAll("private", "")
                .replaceAll("public", "")
                .replaceAll("protected", "")
                .replaceAll("static", "");
    }

    @Override
    public String createClass(String line) {
        this.lastValidConversion = CONVERSION.CLASS;
        line = line.replace("class", "CLASS");
        line = checkForEndLineBrace(line);

        return line;
    }

    @Override
    public String createVariable(String line) {

        Matcher rMatcher = firstWordRegexPattern.matcher(line);

        if (rMatcher.find()){
            // In Java, after removing keywords such as public, private, static, etc... both variables and functions will begin with a Type.
            // If the line does not contain an assignment operator, it must a function
            if (!line.contains("=")){
                return createFunction(line, rMatcher.group(0));
            }

            else {
                return line.replace(rMatcher.group(0), "").replace("=", "←").replace(";", "").trim();
            }

        } else {
            return edgeCaseAttempt(line);
        }

    }

    @Override
    public String createFunction(String line, String type) {
        this.lastValidConversion = CONVERSION.FUNCTION;

        line = line.replace(type, "").trim();
        line = "METHOD " + line;
        line = checkForEndLineBrace(line);

        // Finds and removes the types for all parameters if there are any
        Matcher parameterMatcher = parameterRegexPattern.matcher(line);
        if (parameterMatcher.find()){
            Matcher parameterTypeMatcher = withinParenthesesTypeFinderPattern.matcher(parameterMatcher.group(0));
            if (parameterTypeMatcher.find()){
                for (int i = 0; i < parameterTypeMatcher.groupCount() + 1; i++) {

                    // If the word found is either a java primitive or it's first letter is Capitalized (basically if it's not a variable name)
                    if (primitiveTypes.contains(parameterTypeMatcher.group(i)) || Character.isUpperCase(parameterTypeMatcher.group(i).charAt(0))){
                        line = line.replace(parameterTypeMatcher.group(i), "").trim();
                    }

                    // TODO: Left off here, come up with solution for spacing issue when removing parameter types

                }
            }
        }


        return line;
    }

    @Override
    public String createForLoop(String line) {

        // TODO: Foreach implementation

        this.lastValidConversion = CONVERSION.FOR;

        // Searching for the variable type of the iterator, typically int but on rare occasions can be other types
        String type;
        Pattern pattern = Pattern.compile("\\b(int|float|short|long)\\b");
        Matcher matcher = pattern.matcher(line);

        if (matcher.find()){
            type = matcher.group(0);
        } else {
            type = "";
        }

        // Searching for the iterator's variable name
        pattern = Pattern.compile("(?<=\\b" + type + "\\s)(\\w+)");
        matcher = pattern.matcher(line);

        String varName;
        if (matcher.find()){
            varName = matcher.group(0);
        } else {
            varName = "";
        }

        // Replacing
        line = line.replace("for", "FOR");
        line = line.replace("(", "").replace(")", "")
                .replaceAll(";", "").replace("=", "←");
        line = line.replace("<", "to").replace(">", "to").replace(type, "").replace(varName + "++", "").replace(varName + "--", "");

        int indexOfSecondVarReference = line.indexOf(varName, line.indexOf(varName) + 1);

        if (indexOfSecondVarReference != -1){
            line = line.substring(0, indexOfSecondVarReference) + line.substring(indexOfSecondVarReference + varName.length());
        }

        line = line.trim().replaceAll(" +", " ").replace("  ", "");

        line = checkForEndLineBrace(line);

        return line;
    }

    @Override
    public String createWhileLoop(String line) {

        if (line.contains("do")) {
            this.lastValidConversion = CONVERSION.DO;
        } else {
            this.lastValidConversion = CONVERSION.WHILE;
        }

        return line.replace("while", "WHILE").replace("do", "DO").replace("{", "").replace(";", "");
    }

    @Override
    public String createIfStatement(String line) {

        if (line.contains("else if")){
            return line.replace("else if", "ELSE IF").replace("{", " THEN").replace("}", "").trim();
        } else if (line.contains("if")) {
            this.lastValidConversion = CONVERSION.IF;
            return line.replace("if", "IF").replace("{", " THEN").trim();
        } else {
            this.lastValidConversion = CONVERSION.ELSE;
            return line.replace("else", "ELSE").replace("{", "").replace("}", "").trim();
        }
    }

    @Override
    public String createSwitch(String line) {
        this.lastValidConversion = CONVERSION.SWITCH;

        if (line.contains("switch")){
            return line.replace("switch", "CASE").replace("(", "").replace(")", "").replace("{", "").trim() + " OF";
        } else if (line.contains("case")){
            return line.replace("case", "").trim();
        } else {
            return line.replace("default", "DEFAULT").trim();
        }
    }

    @Override
    public String createBlockBegin(String line) {
        return "BEGIN";
    }

    @Override
    public String createBlockEnd(String line) {

        // Depending on the last valid conversion, a block end might be END, END CLASS, or ENDIF

        switch (this.lastValidConversion) {
            case IF:
                if (line.contains("else")){
                    return "";
                } else {
                    lastValidConversion = CONVERSION.NIL;
                    return "ENDIF";
                }

            case DO: return null;
            case NIL: return "END";
            case ELSE: return "ENDIF";
            case CLASS: return "END CLASS";
            case WHILE: return "ENDWHILE";
            case SWITCH: return "ENDCASE";
            default: return "END";
        }
    }

    @Override
    public String edgeCaseAttempt(String line) {

        // TODO: READ line

        // If the line is an import or package line
        if (line.contains("package") || line.contains("import")){
            return "";
        }

        // Removing override tags completely
        if (line.contains("@Override")){
            return null;
        }

        // If the line contains "return" it must be a return statement
        if (line.contains("return")){
            return line.replace("return", "RETURN").replace(";", "").trim();
        }

        // If the line contains "void" it must be a method signature
        if (line.contains("void")){
            return createFunction(line, "void");
        }

        if (line.contains("if") || line.contains("else if")){
            return createIfStatement(line);
        }

        if (line.contains("break")){
            return line.replace("break", "BREAK").replace(";", "").trim();
        }

        // If the line contains the following, it must be a print statement
        if (line.contains("System.out.print") || line.contains("System.out.format")){

            // Find the index of the first parentheses, and create a substring from there (this removes all variations of
            // System.out.print and System.out.format
            line = line.substring(line.indexOf("(")).replace("(", " ").replace(")", "").replace(";", "").trim();
            return "PRINT " + line;
        }

        // TODO: Solve +=, -=, /= etc...

        // If the line is assigning a variable | i.e. something like `someVar = 4;`
        Matcher matcher = behindAssignmentOperatorRegexPattern.matcher(line);
        if (matcher.find()){
            return line.replace("=", "←").replace(";", "").trim();
        }

        // If none of the above, just return the line itself since it cannot be transpiled
        return line;
    }

    @Override
    public String lastDitchConversion(String line) {
        return line;
    }

    private String checkForEndLineBrace(String line){

        if (line.contains("{") && !line.contains("if")  && !line.contains("else")){
            line = line.replace("{", "");
            line += "\n" + String.join("", Collections.nCopies(currentLineSpacesRemoved, " ")) + "BEGIN";
        }
        return line;
    }

}
