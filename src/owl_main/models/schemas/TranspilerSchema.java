package owl_main.models.schemas;

import java.util.HashMap;
import java.util.function.Function;

abstract public class TranspilerSchema {

    public enum CONVERSION {
        CLASS, FUNCTION, FOR, IF, ELSE, DO, WHILE, SWITCH, NIL
    }

    public HashMap<String, Function<String, String>> guide = new HashMap<>();

    // The amount of spaces removed by the trim() function for the current line being transpiled
    public int currentLineSpacesRemoved = 0;

    public TranspilerSchema() {

        guide.put("class", this::createClass);

        guide.put("int", this::createVariable);
        guide.put("float", this::createVariable);
        guide.put("String", this::createVariable);
        guide.put("double", this::createVariable);
        guide.put("boolean", this::createVariable);
        guide.put("short", this::createVariable);
        guide.put("long", this::createVariable);
        guide.put("char", this::createVariable);

        guide.put("for", this::createForLoop);
        guide.put("do", this::createWhileLoop);
        guide.put("while", this::createWhileLoop);


        guide.put("if", this::createIfStatement);
        guide.put("else", this::createIfStatement);
        guide.put("else if", this::createIfStatement);
        guide.put("switch", this::createSwitch);
        guide.put("default", this::createSwitch);
        guide.put("case", this::createSwitch);

        guide.put("{", this::createBlockBegin);
        guide.put("}", this::createBlockEnd);

    }

    public abstract String getConversion(String match, String line);
    public abstract String cleanCode(String sourceCode);
    public abstract String createClass(String line);
    public abstract String createVariable(String line);
    public abstract String createFunction(String line, String type);
    public abstract String createForLoop(String line);
    public abstract String createWhileLoop(String line);
    public abstract String createIfStatement(String line);
    public abstract String createSwitch(String line);
    public abstract String createBlockBegin(String line);
    public abstract String createBlockEnd(String line);
    public abstract String edgeCaseAttempt(String line);
    public abstract String lastDitchConversion(String line);
}
