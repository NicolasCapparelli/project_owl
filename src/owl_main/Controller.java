package owl_main;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import owl_main.models.schemas.CSharpToPseudoSchema;
import owl_main.models.schemas.KsuPseudocodeSchema;
import owl_main.models.Transpiler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class Controller {
    @FXML
    private AnchorPane homeContainer;

    @FXML
    private MenuBar menuBar;

    @FXML
    private TextArea sourceField;

    @FXML
    private TextArea outField;

    @FXML
    private Button lockScrollBarsButton;

    @FXML
    private Button transpileButton;

    @FXML
    private ImageView lockScrollButtonImage;

    @FXML
    private ImageView transpileButtonImage;

    @FXML
    private ChoiceBox<String> sourcePicker;

    @FXML
    private ChoiceBox<String> outputPicker;

    private File openLockFile = new File("assets/baseline_lock_open_black_48dp.png");
    private File closeLockFile = new File("assets/baseline_lock_black_48dp.png");
    private ScrollBar sourceScrollBar;
    private ScrollBar outputScrollBar;
    private boolean isScrollLocked;

    @FXML
    private void initialize() {

        isScrollLocked = true;

        // Populating the ChoiceBoxes
        sourcePicker.setItems(FXCollections.observableArrayList("Java", "C#"));
        outputPicker.setItems(FXCollections.observableArrayList("Pseudocode"));
        sourcePicker.setValue("C#");
        outputPicker.setValue("Pseudocode");

        // Setting images for the buttons
        File file = new File("assets/baseline_arrow_right_alt_black_48dp.png");
        transpileButtonImage.setImage(new Image(file.toURI().toString()));
        lockScrollButtonImage.setImage(new Image(closeLockFile.toURI().toString()));

        // Setting tooltips
        transpileButton.setTooltip(new Tooltip("Transpile"));
        Tooltip.install(transpileButtonImage, new Tooltip("Transpile"));

        lockScrollBarsButton.setTooltip(new Tooltip("Lock Scroll Bars"));
        Tooltip.install(lockScrollButtonImage, new Tooltip("Lock Scroll Bars"));


        sourceField.setStyle("-fx-font-family: monospace");
        outField.setStyle("-fx-font-family: monospace");

        // Extracts the ScrollBars from each TextArea to use in onLockScrollClicked
        Platform.runLater(() -> {
            Node n = sourceField.lookup(".scroll-bar");
            if (n instanceof ScrollBar) {
                sourceScrollBar = (ScrollBar) n;
                if (sourceScrollBar.getOrientation().equals(Orientation.VERTICAL)) {
                    Node z = outField.lookup(".scroll-bar");
                    outputScrollBar = (ScrollBar) z;
                }
            }

            sourceScrollBar.valueProperty().bindBidirectional(outputScrollBar.valueProperty());
        });


    }

    @FXML
    private void onTranspileClicked(){

        Transpiler transpiler;
        String sourceLang = sourcePicker.getValue();

        switch (sourceLang) {
            case "Java":
                transpiler = new Transpiler(new KsuPseudocodeSchema());
                break;
            case "C#":
                transpiler = new Transpiler(new CSharpToPseudoSchema());
                break;
            default:
                return;
        }

        // String cleanSource = transpiler.cleanSource(sourceField.getText());
        String output = transpiler.transpile(sourceField.getText().split("\n"));

        outField.setText(output);
    }

    @FXML
    private void onLockScrollClicked(){

        // Binds the value properties of each TextArea's scrollbars so they scroll in sync
        if (isScrollLocked){
            sourceScrollBar.valueProperty().unbindBidirectional(outputScrollBar.valueProperty());
            lockScrollButtonImage.setImage(new Image(openLockFile.toURI().toString()));
            isScrollLocked = false;
        } else {
            sourceScrollBar.valueProperty().bindBidirectional(outputScrollBar.valueProperty());
            lockScrollButtonImage.setImage(new Image(closeLockFile.toURI().toString()));
            isScrollLocked = true;
        }
    }

    @FXML
    private void onSaveAs(){

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");

        fileChooser.getExtensionFilters().add(extFilter);
        // FileChooser.ExtensionFilter extFilterTwo =
        //        new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf"); TODO: Implement Save to PDF functionality
        // fileChooser.getExtensionFilters().add(extFilterTwo); TODO: Implement Save to PDF functionality

        // Set placeholder filename
        fileChooser.setInitialFileName("Output");

        //Show save file dialog
        File file = fileChooser.showSaveDialog(homeContainer.getScene().getWindow());

        if(file != null){
            saveFile(file);
        }

    }

    private void saveFile(File file){

        System.out.println();

        // If the user selects .txt
        String fileType = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        if (fileType.equals("txt")){

            // Code source: https://stackoverflow.com/a/18007192/5266445
            ObservableList<CharSequence> paragraph = outField.getParagraphs();
            Iterator<CharSequence> iterator = paragraph.iterator();
            try
            {
                BufferedWriter bf = new BufferedWriter(new FileWriter(file));
                while(iterator.hasNext())
                {
                    CharSequence seq = iterator.next();
                    bf.append(seq);
                    bf.newLine();
                }
                bf.flush();
                bf.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        else if (fileType.equals("pdf")){
            // TODO: Implement Save to PDF functionality
        }

    }


}
