# Project Owl

A Source Code to Kennesaw State University Pseudocode "transpiler" that currently supports Java and C#. 

## Purpose

Kennesaw State University requires students in Intro to Programming classes (I & II) to submit both Source Code and Pseudocode for each assignment. This program allows you to knock them both out at once by using RegEx and string manipulation to take in source code and output equivalent Pseudocode conforming to KSU's standards.


## Features

- Source Language Selection

- Save output to text file

- Save output to PDF (Not yet implemented)

- Synchronous scrolling between Source Code and Output


## Known Bugs

- Scaling the window causes buttons grow or shrink incorrectly due to AnchorPane constraints

- C# Implementation is not quite ready for use


# Active

The project has reached a stable, usable state and is being actively developed.